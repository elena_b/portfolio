# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_01_22_124634) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "previews", force: :cascade do |t|
    t.string "prev_img"
    t.bigint "project_id"
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["project_id"], name: "index_previews_on_project_id"
    t.index ["title"], name: "unique_project_ids", unique: true
  end

  create_table "projects", force: :cascade do |t|
    t.string "title", null: false
    t.string "slug"
    t.string "year", null: false
    t.string "link"
    t.string "screen"
    t.string "company"
    t.string "description"
    t.integer "peoplecount"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company"], name: "index_projects_on_company"
    t.index ["slug"], name: "index_projects_on_slug"
    t.index ["title"], name: "index_projects_on_title"
  end

  create_table "projects_skills", id: false, force: :cascade do |t|
    t.bigint "skill_id", null: false
    t.bigint "project_id", null: false
  end

  create_table "skills", force: :cascade do |t|
    t.string "title", null: false
    t.integer "project_count", default: 0, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["project_count"], name: "index_skills_on_project_count"
    t.index ["title"], name: "index_skills_on_title"
  end

end

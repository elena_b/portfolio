class CreateProjects < ActiveRecord::Migration[7.0]
  def change
    create_table :projects do |t|
      t.string  :title, null: false,  index: true
      t.string  :slug,                index: true
      t.string  :year,  null: false
      t.string  :link
      t.string  :screen
      t.string  :company,            index: true
      t.string  :description
      t.integer :peoplecount
      t.timestamps
    end
  end
end

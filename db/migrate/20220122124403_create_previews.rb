class CreatePreviews < ActiveRecord::Migration[7.0]
  def change
    create_table :previews do |t|
      t.string           :prev_img
      t.references  :project,  index: true
      t.string           :title,    index: { unique: true, name: 'unique_project_ids' }
      t.timestamps
    end
  end
end

class CreateSkills < ActiveRecord::Migration[7.0]
  def change
    create_table :skills do |t|
      t.string  :title,         null: false,             index: true
      t.integer :project_count, null: false, default: 0, index: true
      t.timestamps
    end
  end
end

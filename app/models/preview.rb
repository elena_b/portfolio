class Preview < ApplicationRecord
  mount_uploader :prev_img, PreviewUploader
  belongs_to :project

  validates :title, uniqueness: true
  validates :prev_img, :title, :project_id, presence: true

end

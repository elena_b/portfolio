class Project < ApplicationRecord

  before_validation :prepare_slug

  has_many :previews, dependent: :destroy

  has_and_belongs_to_many :skills

  validates :title, uniqueness: true
  validates :title, :slug, :year, presence: true

  def prepare_slug
    self.slug = I18n.transliterate(self.title) if self.slug.blank?
  end

end

class BaseImageUploader < BaseUploader
  include CarrierWave::RMagick
  # Add an allowlist of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  # def extension_allowlist
  def extension_white_list
    %w(jpg jpeg gif png svg)
  end
end

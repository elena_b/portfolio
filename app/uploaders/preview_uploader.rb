class PreviewUploader < BaseImageUploader
  version :thumb do
    process resize_and_pad: [220, 300, "#fff", Magick::CenterGravity]
  end
end

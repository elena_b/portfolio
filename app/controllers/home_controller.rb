class HomeController < ApplicationController

  def index
    begin
      if File.file?("#{Rails.root}/public/about.html")
        about_file = File.open("#{Rails.root}/public/about.html")
        @about = Nokogiri::HTML(about_file, nil, Encoding::UTF_8.to_s)
      end
      @projects = Project.all
    rescue
      ActionController::RoutingError.new('Извините. Что-то пошло не так.')
    end
  end

end

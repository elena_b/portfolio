# README

This README is for my own project to try and learn some new technologies:

* Ruby version is 3.0.3 and rails version is 7.0.1

* Please install postgres, for example you can use https://postgresapp.com/ 
  
  and then create a database with command ```db:create db:migrate```
  
  Database initialization in ```config/database.yml```

* run ```bundle install``` 

* run ```bin/rake parse_files:get_projects``` to get information about projects from file ```public/projects.xml```

  and than run ```whenever --update-crontab``` to get update about projects, every 10 minutes

* main branch on this project is ```main```
  
* no deployment and no test yet

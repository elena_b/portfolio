require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Portfolio
  class Application < Rails::Application
    config.load_defaults 7.0
    config.site_name = 'Portfolio'
    config.time_zone = 'Krasnoyarsk'
    config.site_link = 'tac24.ru'
    config.i18n.default_locale = :ru
    config.i18n.available_locales = [:en,:ru]
  end
end

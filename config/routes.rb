Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  get 'home/data', to: 'home#data'
  post 'home/upload', to: 'home#upload'
  post 'home/destroy', to: 'home#destroy'

  root "home#index"
end

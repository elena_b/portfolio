require 'nokogiri'

namespace :parse_files do

  desc 'parse projects from xml file'
  task get_projects: :environment do
    project_doc = Nokogiri::XML(File.open("#{Rails.root}/public/projects.xml"))
    # сделать проверку на существование файла и на то что данные в файле корректные
    project_doc.xpath('root/projects/project').each do |xml_project|
      current_project = Project.where(title: xml_project.attr('title').to_s).first_or_create do |project|
        project.title = xml_project.attr('title')
        project.link = xml_project.attr('link')
        project.year = xml_project.attr('year')
        project.company = xml_project.attr('company')
        project.description = xml_project.attr('description')
        project.peoplecount = xml_project.attr('peoplecount')
        project.save
      end
      xml_project.xpath('techskills/techskill').each do |xml_skill|
        Skill.where(title: xml_skill.attr('title').to_s).first_or_create do |skill|
          skill.title = xml_skill.attr('title')
          # счетчик для проверки что тест валится с ошибкой
          skill.project_count += 1
        end
      end
      xml_project.xpath('screens/screen').each do |xml_screen|
        if File.file?("#{Rails.root}/public#{xml_screen.attr('img')}")
          project_id = current_project.id
          current_file = File.open("#{Rails.root}/public#{xml_screen.attr('img')}")
          current_file_name = File.basename(current_file.path)
          Preview.where(project_id: project_id, title: current_file_name).first_or_create do |screen|
            screen.title = current_file_name
            screen.prev_img = current_file
            screen.project_id = project_id
          end
        end
      end
    end

  end
  desc 'end parse projects from xml file'

  desc 'parse about info from html file'
  task get_about_info: :environment do
    about_doc = File.open("#{Rails.root}/public/about.html") { |f| Nokogiri::XML(f) }
  end
  desc 'end parse about info from html file'

end
